import {eventSource, EventSourceConfiguration, TriggeredEvent, State, InternalTriggeredEvent, Command} from '../src'
import {Empty, Optional} from '@othree.io/optional'

describe('EventSource', () => {
    const configuration: EventSourceConfiguration = {
        SnapshotEventType: 'SNAPSHOT',
        SnapshotFrequency: 10,
    }

    interface Wallet {
        readonly value: number
        readonly message: string
    }

    const initialState: Wallet = {
        value: 0,
        message: 'Calculated value is:',
    }

    interface CalculatorEvent {
        readonly contextId: string
        readonly type: string
        readonly body: number
    }

    const reduce = (state: Wallet, event: TriggeredEvent<CalculatorEvent>): Wallet => {
        switch (event.type) {
            case 'Initialized':
                return {
                    ...state,
                    value: event.body,
                }
            case 'ValueAdded':
                return {
                    ...state,
                    value: state.value + event.body,
                }
            case 'ValueSubstracted':
                return {
                    ...state,
                    value: state.value - event.body,
                }
        }
        return state
    }

    const newId = () => '1337'

    const now = () => 9000

    describe('loadState', () => {
        it('should return the initial state if no context id is provided', async () => {
            const getEvents = jest.fn()
            const getInitialState = jest.fn().mockResolvedValue(Optional(initialState))
            const maybeState = await eventSource.loadState(configuration)(getEvents)<Wallet>(getInitialState)(reduce)(Empty())

            expect(maybeState.isPresent).toBeTruthy()
            expect(maybeState.get()).toStrictEqual({
                state: {
                    value: 0,
                    message: 'Calculated value is:',
                },
                events: [],
            })
        })

        it('should return the initial state if the context id has no events', async () => {
            const getEvents = jest.fn().mockReturnValue(Promise.resolve(Optional([])))
            const getInitialState = jest.fn().mockResolvedValue(Optional(initialState))
            const maybeState = await eventSource.loadState(configuration)(getEvents)<Wallet>(getInitialState)(reduce)(Optional('1337'))

            expect(maybeState.isPresent).toBeTruthy()
            expect(maybeState.get()).toStrictEqual({
                state: {
                    value: 0,
                    message: 'Calculated value is:',
                },
                events: [],
            })

            expect(getEvents).toBeCalledTimes(1)
            expect(getEvents).toBeCalledWith('1337')
        })

        it('should return the reduced state if the context id has events and no snapshot', async () => {

            const getEvents = jest.fn().mockReturnValue(Promise.resolve(Optional<Array<InternalTriggeredEvent>>([
                {
                    contextId: '1337',
                    eventId: '003',
                    eventDate: 3,
                    type: 'ValueSubstracted',
                    body: 3,
                },
                {
                    contextId: '1337',
                    eventId: '002',
                    eventDate: 2,
                    type: 'ValueAdded',
                    body: 10,
                },
                {
                    contextId: '1337',
                    eventId: '001',
                    eventDate: 1,
                    type: 'Initialized',
                    body: 1,
                },
            ])))
            const getInitialState = jest.fn().mockResolvedValue(Optional(initialState))
            const maybeState = await eventSource.loadState(configuration)(getEvents)<Wallet>(getInitialState)(reduce)(Optional('1337'))

            expect(maybeState.get()).toStrictEqual({
                state: {value: 8, message: 'Calculated value is:'},
                events: [
                    {
                        contextId: '1337',
                        eventId: '001',
                        eventDate: 1,
                        type: 'Initialized',
                        body: 1,
                    },
                    {
                        contextId: '1337',
                        eventId: '002',
                        eventDate: 2,
                        type: 'ValueAdded',
                        body: 10,
                    },
                    {
                        contextId: '1337',
                        eventId: '003',
                        eventDate: 3,
                        type: 'ValueSubstracted',
                        body: 3,
                    },
                ],
            })
            expect(maybeState.isPresent).toBeTruthy()

            expect(getEvents).toBeCalledTimes(1)
            expect(getEvents).toBeCalledWith('1337')
        })

        it('should return the reduced state playing event from the closest snapshot', async () => {
            const getEvents = jest.fn().mockReturnValue(Promise.resolve(Optional<Array<InternalTriggeredEvent>>([
                {
                    contextId: '1337',
                    eventId: '003',
                    eventDate: 3,
                    type: 'ValueAdded',
                    body: 100,
                },
                {
                    contextId: '1337',
                    eventId: '002',
                    eventDate: 2,
                    type: 'ValueAdded',
                    body: 500,
                },
                {
                    contextId: '1337',
                    eventId: '001',
                    eventDate: 1,
                    type: configuration.SnapshotEventType,
                    body: {},
                    snapshot: {
                        value: 1500,
                    },
                },
                {
                    contextId: '1337',
                    eventId: '16n0r3d',
                    eventDate: 3,
                    type: 'ValueAdded',
                    body: 340,
                },
            ])))
            const getInitialState = jest.fn().mockResolvedValue(Optional(initialState))
            const maybeState = await eventSource.loadState(configuration)(getEvents)<Wallet>(getInitialState)(reduce)(Optional('1337'))

            expect(maybeState.get()).toStrictEqual({
                state: {value: 2100, message: 'Calculated value is:'},
                events: [
                    {
                        contextId: '1337',
                        eventId: '002',
                        eventDate: 2,
                        type: 'ValueAdded',
                        body: 500,
                    },
                    {
                        contextId: '1337',
                        eventId: '003',
                        eventDate: 3,
                        type: 'ValueAdded',
                        body: 100,
                    },
                ],
            })
            expect(maybeState.isPresent).toBeTruthy()

            expect(getEvents).toBeCalledTimes(1)
            expect(getEvents).toBeCalledWith('1337')
        })
    })

    describe('calculateNewState', () => {
        interface CalculatorEvent {
            readonly contextId: string
            readonly type: string
            readonly body: number
        }

        it('should persist the new events and snapshot and return the new reduced state', async () => {
            const configuration: EventSourceConfiguration = {
                SnapshotEventType: 'SNAPSHOT',
                SnapshotFrequency: 3,
            }

            const command: Command = {
                contextId: '1',
                type: 'RunOperations',
                body: {},
                metadata: {
                    user: 'chuck.norris',
                },
                emitter: {
                    clientId: 'cart.app',
                    emitterId: 'chuck.norris',
                },
            }

            const firstExpectedEvent: TriggeredEvent<CalculatorEvent> = {
                contextId: '1',
                type: 'ValueAdded',
                eventDate: 9000,
                eventId: '1337',
                body: 300,
                metadata: {
                    user: 'chuck.norris',
                },
                emitter: {
                    clientId: 'cart.app',
                    emitterId: 'chuck.norris',
                },
            }
            const secondExpectedEvent: TriggeredEvent<CalculatorEvent> = {
                type: configuration.SnapshotEventType,
                contextId: '1',
                eventDate: 9000,
                eventId: '1337',
                snapshot: {value: 1300, message: 'The value is:'},
                body: {} as any,
            }
            const thirdExpectedEvent: TriggeredEvent<CalculatorEvent> = {
                contextId: '1',
                type: 'ValueSubstracted',
                eventDate: 9000,
                eventId: '1337',
                body: 150,
                metadata: {
                    user: 'chuck.norris',
                },
                emitter: {
                    clientId: 'cart.app',
                    emitterId: 'chuck.norris',
                },
            }

            const persistEvent = jest.fn().mockResolvedValueOnce(Optional(firstExpectedEvent))
                .mockResolvedValueOnce(Optional(secondExpectedEvent))
                .mockResolvedValueOnce(Optional(thirdExpectedEvent))

            const state: State<Wallet> = {
                state: {
                    value: 1000,
                    message: 'The value is:',
                },
                events: [
                    {contextId: '1', type: 'ValueAdded', eventDate: 9000, eventId: '5432', body: 1000},
                ],
            }

            const maybeNewState = await eventSource.calculateNewState(configuration)(newId)(now)(persistEvent)(reduce)(
                command,
                state,
                [
                    {contextId: '1', type: 'ValueAdded', body: 300},
                    {contextId: '1', type: 'ValueSubstracted', body: 150},
                ],
            )

            expect(maybeNewState.isPresent).toBeTruthy()
            expect(maybeNewState.get()).toStrictEqual({
                state: {
                    value: 1150,
                    message: 'The value is:',
                },
                events: [
                    {
                        contextId: '1',
                        type: 'ValueSubstracted',
                        eventDate: 9000,
                        eventId: '1337',
                        body: 150,
                        metadata: {user: 'chuck.norris'},
                        emitter: {
                            clientId: 'cart.app',
                            emitterId: 'chuck.norris',
                        },
                    },
                ],
                newEvents: [
                    {
                        contextId: '1',
                        type: 'ValueAdded',
                        eventDate: 9000,
                        eventId: '1337',
                        body: 300,
                        metadata: {user: 'chuck.norris'},
                        emitter: {
                            clientId: 'cart.app',
                            emitterId: 'chuck.norris'
                        }
                    },
                    {
                        contextId: '1',
                        type: 'ValueSubstracted',
                        eventDate: 9000,
                        eventId: '1337',
                        body: 150,
                        metadata: {user: 'chuck.norris'},
                        emitter: {
                            clientId: 'cart.app',
                            emitterId: 'chuck.norris'
                        }
                    },
                ],
            })
            expect(persistEvent).toBeCalledTimes(3)
            expect(persistEvent).toBeCalledWith(firstExpectedEvent)
            expect(persistEvent).toBeCalledWith(secondExpectedEvent)
            expect(persistEvent).toBeCalledWith(thirdExpectedEvent)
        })

        it('should persist the new events without metadata and snapshot and return the new reduced state', async () => {
            const configuration: EventSourceConfiguration = {
                SnapshotEventType: 'SNAPSHOT',
                SnapshotFrequency: 3,
            }

            const command: Command = {
                contextId: '1',
                type: 'RunOperations',
                body: {},
            }

            const firstExpectedEvent: InternalTriggeredEvent = {
                contextId: '1',
                type: 'ValueAdded',
                eventDate: 9000,
                eventId: '1337',
                body: 300,
            }
            const secondExpectedEvent: InternalTriggeredEvent = {
                type: configuration.SnapshotEventType,
                contextId: '1',
                eventDate: 9000,
                eventId: '1337',
                snapshot: {value: 1300, message: 'The value is:'},
                body: {},
            }
            const thirdExpectedEvent: InternalTriggeredEvent = {
                contextId: '1',
                type: 'ValueSubstracted',
                eventDate: 9000,
                eventId: '1337',
                body: 150,
            }

            const persistEvent = jest.fn().mockResolvedValueOnce(Optional(firstExpectedEvent))
                .mockResolvedValueOnce(Optional(secondExpectedEvent))
                .mockResolvedValueOnce(Optional(thirdExpectedEvent))

            const state: State<Wallet> = {
                state: {
                    value: 1000,
                    message: 'The value is:',
                },
                events: [
                    {contextId: '1', type: 'ValueAdded', eventDate: 9000, eventId: '5432', body: 1000},
                ],
            }

            const maybeNewState = await eventSource.calculateNewState(configuration)(newId)(now)(persistEvent)(reduce)(
                command,
                state,
                [
                    {contextId: '1', type: 'ValueAdded', body: 300},
                    {contextId: '1', type: 'ValueSubstracted', body: 150},
                ],
            )

            expect(maybeNewState.isPresent).toBeTruthy()
            expect(maybeNewState.get()).toStrictEqual({
                state: {
                    value: 1150,
                    message: 'The value is:',
                },
                events: [
                    {contextId: '1', type: 'ValueSubstracted', eventDate: 9000, eventId: '1337', body: 150},
                ],
                newEvents: [
                    {contextId: '1', type: 'ValueAdded', eventDate: 9000, eventId: '1337', body: 300},
                    {contextId: '1', type: 'ValueSubstracted', eventDate: 9000, eventId: '1337', body: 150},
                ],
            })
            expect(persistEvent).toBeCalledTimes(3)
            expect(persistEvent).toBeCalledWith(firstExpectedEvent)
            expect(persistEvent).toBeCalledWith(secondExpectedEvent)
            expect(persistEvent).toBeCalledWith(thirdExpectedEvent)
        })
    })
})
