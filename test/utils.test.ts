import {utils} from '../src'

describe('now', () => {
    const globalNow = Date.now

    beforeAll(() => {
        Date.now = () => 9000
    })

    afterAll(() => {
        Date.now = globalNow
    })

    it('date provider should return the current date', () => {
        const rightNow = utils.now()

        expect(rightNow).toEqual(9000)
    })
})
