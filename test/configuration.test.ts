import {configuration} from '../src'

describe('EnvVarConfiguration', () => {
    it('should return the default configuration', () => {
        delete process.env.CHISEL_SNAPSHOT_EVENT_TYPE
        delete process.env.CHISEL_SNAPSHOT_FREQUENCY

        const conf = configuration.getEnvVarConfiguration()

        expect(conf).toStrictEqual({
            SnapshotEventType: '$SNAPSHOT$',
            SnapshotFrequency: 100
        })
    })

    it('should return the configured values', () => {
        process.env.CHISEL_SNAPSHOT_EVENT_TYPE = '$THE_SNAPSHOT$'
        process.env.CHISEL_SNAPSHOT_FREQUENCY = '500'

        const conf = configuration.getEnvVarConfiguration()

        expect(conf).toStrictEqual({
            SnapshotEventType: '$THE_SNAPSHOT$',
            SnapshotFrequency: 500
        })
    })
})
