import {actor, Command, ChiselEvent} from '../src'
import {Optional} from '@othree.io/optional'

describe('Actor', () => {
    interface Wallet {
        readonly value: number
    }

    describe('handle', () => {
        it('should handle the specified command', async () => {
            const loadState = jest.fn().mockResolvedValue(Optional({
                state: {
                    value: 0,
                },
                events: [],
            }))

            const handleCommand: actor.HandleCommand<Wallet> = async (state: Wallet, command: Command): Promise<Optional<ChiselEvent | Array<ChiselEvent>>> => {
                if (command.type === 'AddValue') {
                    return Optional({
                        contextId: command.contextId!,
                        type: 'ValueAdded',
                        body: command.body,
                    })
                }

                return undefined!
            }

            const calculateNewState = jest.fn().mockResolvedValue(Optional({
                state: {
                    value: 1,
                },
                events: [{
                    contextId: '1337',
                    eventId: '001',
                    eventDate: 3,
                    type: 'ValueAdded',
                    body: 1,
                }],
                newEvents: [{
                    contextId: '1337',
                    eventId: '001',
                    eventDate: 3,
                    type: 'ValueAdded',
                    body: 1,
                }],
            }))

            const publishEvent = jest.fn()
                .mockReturnValueOnce(Promise.resolve(Optional(true)))
                .mockReturnValueOnce(Promise.resolve(Optional(false)))

            const command: Command = {
                contextId: '1337',
                type: 'AddValue',
                body: 1,
                metadata: {
                    user: 'chuck.norris',
                },
            }

            const maybeState = await actor.handle<Wallet>(loadState)(handleCommand)(calculateNewState)(publishEvent)(command)

            expect(maybeState.isPresent).toBeTruthy()
            expect(maybeState.get()).toStrictEqual({
                state: {
                    value: 1,
                },
                events: [{
                    contextId: '1337',
                    eventId: '001',
                    eventDate: 3,
                    type: 'ValueAdded',
                    body: 1,
                }],
            })

            expect(loadState).toBeCalledTimes(1)
            expect((loadState.mock.calls[0][0]).isPresent).toBeTruthy()
            expect((loadState.mock.calls[0][0]).get()).toEqual('1337')

            expect(calculateNewState).toBeCalledTimes(1)
            expect(calculateNewState).toBeCalledWith(
                command,
                {
                    state: {
                        value: 0,
                    },
                    events: [],
                }, [{
                    contextId: command.contextId!,
                    type: 'ValueAdded',
                    body: command.body,
                }])

            expect(publishEvent).toBeCalledTimes(1)
            expect(publishEvent).toBeCalledWith({
                contextId: '1337',
                eventDate: 3,
                eventId: '001',
                type: 'ValueAdded',
                body: 1,
            }, {
                value: 1,
            })
        })

        it('should handle the specified command when it returns more than 1 event', async () => {
            const loadState = jest.fn().mockResolvedValue(Optional({
                state: {
                    value: 0,
                },
                events: [],
            }))

            const calculateNewState = jest.fn().mockResolvedValue(Optional({
                state: {
                    value: 2,
                },
                events: [{
                    contextId: '1337',
                    eventId: '001',
                    eventDate: 3,
                    type: 'ValueAdded',
                    body: 1,
                }, {
                    contextId: '1337',
                    eventId: '002',
                    eventDate: 3,
                    type: 'ValueAdded',
                    body: 2,
                }],
                newEvents: [{
                    contextId: '1337',
                    eventId: '001',
                    eventDate: 3,
                    type: 'ValueAdded',
                    body: 1,
                }, {
                    contextId: '1337',
                    eventId: '002',
                    eventDate: 3,
                    type: 'ValueAdded',
                    body: 2,
                }],
            }))

            const handleCommand: actor.HandleCommand<Wallet> = async (state: Wallet, command: Command): Promise<Optional<ChiselEvent | Array<ChiselEvent>>> => {
                if (command.type === 'AddValue') {
                    return Optional([{
                        contextId: command.contextId!,
                        type: 'ValueAdded',
                        body: 1,
                    }, {
                        contextId: command.contextId!,
                        type: 'ValueAdded',
                        body: 2,
                    }])
                }

                return undefined!
            }

            const publishEvent = jest.fn()
                .mockReturnValueOnce(Promise.resolve(Optional(true)))
                .mockReturnValueOnce(Promise.resolve(Optional(false)))

            const command: Command = {
                contextId: '1337',
                type: 'AddValue',
                body: 1,
            }

            const maybeState = await actor.handle<Wallet>(loadState)(handleCommand)(calculateNewState)(publishEvent)(command)

            expect(maybeState.isPresent).toBeTruthy()
            expect(maybeState.get()).toStrictEqual({
                state: {
                    value: 2,
                },
                events: [{
                    contextId: '1337',
                    eventId: '001',
                    eventDate: 3,
                    type: 'ValueAdded',
                    body: 1,
                }, {
                    contextId: '1337',
                    eventId: '002',
                    eventDate: 3,
                    type: 'ValueAdded',
                    body: 2,
                }],
            })

            expect(loadState).toBeCalledTimes(1)
            expect((loadState.mock.calls[0][0]).isPresent).toBeTruthy()
            expect((loadState.mock.calls[0][0]).get()).toEqual('1337')

            expect(calculateNewState).toBeCalledTimes(1)
            expect(calculateNewState).toBeCalledWith(
                command,
                {
                    state: {
                        value: 0,
                    },
                    events: [],
                }, [{
                    contextId: command.contextId!,
                    type: 'ValueAdded',
                    body: 1,
                }, {
                    contextId: command.contextId!,
                    type: 'ValueAdded',
                    body: 2,
                }])

            expect(publishEvent).toBeCalledTimes(2)
            expect(publishEvent).toBeCalledWith({
                contextId: '1337',
                eventDate: 3,
                eventId: '001',
                type: 'ValueAdded',
                body: 1,
            }, {
                value: 2,
            })
            expect(publishEvent).toBeCalledWith({
                contextId: '1337',
                eventDate: 3,
                eventId: '002',
                type: 'ValueAdded',
                body: 2,
            }, {
                value: 2,
            })
        })
    })
})
