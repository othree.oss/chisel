import {EventSourceConfiguration} from './types'
import {Optional} from '@othree.io/optional'

export function getEnvVarConfiguration(): EventSourceConfiguration {
    return Object.freeze({
        SnapshotEventType: Optional(process.env.CHISEL_SNAPSHOT_EVENT_TYPE).orElse('$SNAPSHOT$'),
        SnapshotFrequency: Optional(process.env.CHISEL_SNAPSHOT_FREQUENCY).map(_ => Number(_)).orElse(100)
    })
}
