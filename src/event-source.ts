import {Optional, TryAsync} from '@othree.io/optional'
import {
    ChiselEvent,
    TriggeredEvent,
    EventSourceConfiguration,
    State,
    NewState,
    Command,
    InternalTriggeredEvent,
} from './types'
import {match} from '@othree.io/cerillo'

export type PersistEvent = (event: InternalTriggeredEvent) => Promise<Optional<InternalTriggeredEvent>>
export type Reduce<AggregateRoot, Event extends ChiselEvent> = (state: AggregateRoot, event: TriggeredEvent<Event>) => AggregateRoot

export type LoadState<AggregateRoot> = (contextId: Optional<string>) => Promise<Optional<State<AggregateRoot>>>
export const loadState = (configuration: EventSourceConfiguration) =>
    (getEvents: (contextId: string) => Promise<Optional<Array<InternalTriggeredEvent>>>) =>
        <AggregateRoot>(getInitialState: (contextId: Optional<string>) => Promise<Optional<AggregateRoot>>) =>
            <Event extends ChiselEvent>(reduce: Reduce<AggregateRoot, Event>): LoadState<AggregateRoot> =>
                async (contextId: Optional<string>): Promise<Optional<State<AggregateRoot>>> => {
                    return TryAsync(async () => {
                        if (contextId.isEmpty) {
                            return (await getInitialState(contextId))
                                .map(state => ({
                                    state: state,
                                    events: [],
                                }))
                        }

                        return (await getEvents(contextId.get()))
                            .mapAsync(async events => {
                                if (events.length === 0) {
                                    return (await getInitialState(contextId))
                                        .map(state => ({
                                            state: state,
                                            events: [],
                                        }))
                                }

                                const snapshotIdx = events.findIndex(event => event.type === configuration.SnapshotEventType)

                                const initialState = await getInitialState(contextId)

                                const maybeInitialState = await match<number, Promise<Optional<AggregateRoot>>>(snapshotIdx)
                                    .when(_ => _ < 0).then(async _ => initialState)
                                    .default(async _ => {
                                        return initialState.map(state => {
                                            return {
                                                ...state,
                                                ...events[_].snapshot
                                            }
                                        })
                                    })
                                    .get()

                                return maybeInitialState.map(initialState => {
                                    const eventsReplayRange = snapshotIdx < 0 ? events.length : snapshotIdx

                                    const eventsToReplay = Array.from(Array(eventsReplayRange).keys())
                                        .map(key => events[key])
                                        .reverse()

                                    const currentState: AggregateRoot = eventsToReplay.reduce(reduce, initialState)

                                    return {
                                        state: currentState,
                                        events: eventsToReplay,
                                    }
                                })
                            })
                    })
                }

export type CalculateNewState<AggregateRoot> = (command: Command, state: State<AggregateRoot>, newEvents: Array<ChiselEvent>) => Promise<Optional<NewState<AggregateRoot>>>
export const calculateNewState = (configuration: EventSourceConfiguration) =>
    (newId: () => string) =>
        (now: () => number) =>
            (persistEvent: PersistEvent) =>
                <AggregateRoot, Event extends ChiselEvent>(reduce: Reduce<AggregateRoot, Event>): CalculateNewState<AggregateRoot> =>
                    async (command: Command, state: State<AggregateRoot>, newEvents: Array<ChiselEvent>): Promise<Optional<NewState<AggregateRoot>>> => {
                        return TryAsync(async () => {
                            return newEvents.reduce(async (acumPromise: Promise<NewState<AggregateRoot>>, newEvent: ChiselEvent): Promise<NewState<AggregateRoot>> => {
                                const currentState: NewState<AggregateRoot> = await acumPromise

                                const event = (await persistEvent({
                                    type: newEvent.type,
                                    body: newEvent.body,
                                    contextId: newEvent.contextId,
                                    eventId: newId(),
                                    eventDate: now(),
                                    metadata: command.metadata,
                                    emitter: command.emitter
                                })).get()

                                if ((currentState.events.length + 1) === (configuration.SnapshotFrequency - 1)) {
                                    const snapshot: AggregateRoot = [event].reduce(reduce, currentState.state)

                                    ;(await persistEvent({
                                        snapshot: snapshot,
                                        contextId: event.contextId,
                                        eventId: newId(),
                                        eventDate: now(),
                                        type: configuration.SnapshotEventType,
                                        body: {},
                                    })).get()

                                    return {
                                        state: snapshot,
                                        events: [],
                                        newEvents: [
                                            ...currentState.newEvents,
                                            event,
                                        ],
                                    }
                                } else {
                                    const updatedState = [event].reduce(reduce, currentState.state)

                                    return {
                                        state: updatedState,
                                        events: [
                                            ...currentState.events,
                                            event,
                                        ],
                                        newEvents: [
                                            ...currentState.newEvents,
                                            event,
                                        ],
                                    }
                                }
                            }, Promise.resolve({
                                ...state,
                                newEvents: [],
                            }))
                        })
                    }
