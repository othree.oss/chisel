export interface ChiselEvent {
    readonly contextId: string
    readonly type: string
    readonly body: any
    readonly snapshot?: any
}

export interface Emitter {
    readonly clientId: string
    readonly emitterId: string
}

export interface InternalTriggeredEvent extends ChiselEvent {
    readonly eventId: string
    readonly eventDate: number
    readonly emitter?: Emitter
    readonly metadata?: Record<string, string>
}

export type TriggeredEvent<T extends ChiselEvent> = T & Omit<InternalTriggeredEvent, 'body'>

export interface EventSourceConfiguration {
    readonly SnapshotEventType: string,
    readonly SnapshotFrequency: number
}

export interface Command {
    readonly contextId?: string
    readonly type: string,
    readonly body: any
    readonly emitter?: Emitter
    readonly metadata?: Record<string, string>
}

export interface CommandResult<AggregateRoot> {
    readonly state: AggregateRoot
    readonly events: Array<InternalTriggeredEvent>
}

export interface State<AggregateRoot> {
    readonly state: AggregateRoot,
    readonly events: Array<InternalTriggeredEvent>
}

export interface NewState<AggregateRoot> extends State<AggregateRoot> {
    readonly newEvents: Array<InternalTriggeredEvent>
}
