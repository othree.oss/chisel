import {CalculateNewState, LoadState} from './event-source'
import {Optional} from '@othree.io/optional'
import {Command, CommandResult, ChiselEvent, InternalTriggeredEvent} from './types'
import {match} from '@othree.io/cerillo'


export type HandleCommand<AggregateRoot> = (state: AggregateRoot, command: Command) => Promise<Optional<ChiselEvent | Array<ChiselEvent>>>
export type PublishEvent<AggregateRoot> = (event: InternalTriggeredEvent, state: AggregateRoot) => Promise<Optional<boolean>>
export type Handle<AggregateRoot> = (command: Command) => Promise<Optional<CommandResult<AggregateRoot>>>


export const handle = <AggregateRoot>(loadState: LoadState<AggregateRoot>) =>
    (handleCommand: HandleCommand<AggregateRoot>) =>
        (calculateNewState: CalculateNewState<AggregateRoot>) =>
            (publishEvent: PublishEvent<AggregateRoot>): Handle<AggregateRoot> =>
                async (command: Command): Promise<Optional<CommandResult<AggregateRoot>>> => {
                    return (await loadState(Optional(command.contextId)))
                        .mapAsync(async state => {
                            return (await handleCommand(state.state, command))
                                .mapAsync(async events => {
                                    const theEvents = match<ChiselEvent | Array<ChiselEvent>, Array<ChiselEvent>>(events)
                                        .when(_ => Array.isArray(_)).then(_ => _ as any)
                                        .default(_ => ([_ as any]))
                                        .get()

                                    return (await calculateNewState(command, state, theEvents))
                                        .mapAsync(async newState => {
                                            const notifications = newState.newEvents.map(async event => publishEvent(event, newState.state))

                                            await Promise.allSettled(notifications)

                                            return {
                                                state: newState.state,
                                                events: newState.newEvents
                                            }
                                        })
                                })
                        })
                }
