const aggregate = require('./commands/aggregate')
const config = require('./config/config')
const eventSourcing = require('./events/event-sourcing')
const eventRepository = require('./events/dynamo-event-repository')
const dateProvider = require('./utils/date-provider')
const eventCreator = require('./events/event-creator')
const snsEventNotifier = require('./events/sns-event-notifier')
const commandError = require('./commands/command-error')

module.exports = {
    ...aggregate,
    ...config,
    ...eventSourcing,
    ...eventRepository,
    ...dateProvider,
    ...eventCreator,
    ...snsEventNotifier,
    ...commandError
}